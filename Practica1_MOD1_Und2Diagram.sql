﻿DROP DATABASE IF EXISTS Practica1Mod1; 
CREATE DATABASE Practica1Mod1;
USE Practica1Mod1;

CREATE TABLE clientes (
cod int AUTO_INCREMENT,
nombre varchar (100),
PRIMARY KEY (cod)
  );

CREATE TABLE coches (
id int,
codCliente int AUTO_INCREMENT,
marca varchar(100),
fecha date NOT NULL,
precio float,
PRIMARY KEY (id),
CONSTRAINT fkclientes FOREIGN KEY (codCliente) REFERENCES clientes (cod) ON DELETE RESTRICT ON UPDATE RESTRICT
);

-- introducir datos

  INSERT INTO clientes 
    (cod, nombre) VALUES 
    (1, 'Marcos Perez'),
    (2, 'Maria OViedo'),
    (3, 'Raul Hernandez'),
    (4, 'Martin Otero');

  INSERT INTO coches 
    (id, codCliente, marca, fecha, precio) VALUES 
      (1, 1,   'Renault',  '2021/03/27', 3000),
      (2, 2,   'Kia',      '2021/04/17', 4000),
      (3, 2,   'Toyota',   '2020/12/23', 6000),
      (4, 4,   'Volkswagen', '2021/06/22', 5500) ;

  SELECT * FROM clientes c;
  SELECT * FROM coches c;

